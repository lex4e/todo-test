<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220606152828 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            !$this->connection->getDatabasePlatform() instanceof \Doctrine\DBAL\Platforms\MySQL57Platform,
            "Migration can only be executed safely on '\Doctrine\DBAL\Platforms\MySQL57Platform'."
        );

        $this->addSql('CREATE TABLE author (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(180) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, roles JSON NOT NULL, password VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, UNIQUE INDEX UNIQ_BDAFD8C8F85E0677 (username), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql("INSERT INTO `author` VALUES (1,'Author1','[]','pass'),(3,'Author2','[]','pass'),(5,'Author3','[]','pass'),(7,'Author4','[]','pass'),(9,'Author11','[]','')");

        $this->abortIf(
            !$this->connection->getDatabasePlatform() instanceof \Doctrine\DBAL\Platforms\MySQL57Platform,
            "Migration can only be executed safely on '\Doctrine\DBAL\Platforms\MySQL57Platform'."
        );

        $this->addSql('CREATE TABLE todos (id INT AUTO_INCREMENT NOT NULL, parent_id INT DEFAULT NULL, author_id INT DEFAULT NULL, title VARCHAR(127) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, description LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, status VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, priority INT DEFAULT NULL, created DATETIME DEFAULT NULL, INDEX IDX_CD826255727ACA70 (parent_id), INDEX IDX_CD826255F675F31B (author_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql("INSERT INTO `todos` (id, parent_id, title, description, status, priority, author_id, created) VALUES (7,NULL,'T777','d5','TODO',3,1,'2022-02-11 00:00:00'),(8,7,'T8888','d5','TODO',5,1,'2022-02-02 00:00:00'),(9,8,'T9','d5','TODO',1,1,'2022-05-29 15:55:32'),(10,NULL,'T10','d5','TODO',1,1,'2022-05-29 16:06:41'),(13,7,'T8888','d5','TODO',5,1,'2022-06-05 16:47:22'),(25,NULL,'T8888','d5','TODO',5,3,'2022-06-06 07:21:26'),(32,7,'T8888777__','d5','DONE',5,1,'2022-06-06 09:52:27'),(33,7,'T8888777__1','d5','TODO',5,1,'2022-06-06 14:14:06'),(34,7,'T8888777__2','d5','TODO',5,1,'2022-06-06 14:14:27'),(35,7,'T8888777__2','d5','TODO',5,1,'2022-06-06 14:14:34'),(36,7,'T8888777__2','d5','DONE',5,1,'2022-06-06 15:07:46')");

    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            !$this->connection->getDatabasePlatform() instanceof \Doctrine\DBAL\Platforms\MySQL57Platform,
            "Migration can only be executed safely on '\Doctrine\DBAL\Platforms\MySQL57Platform'."
        );

        $this->addSql('DROP TABLE author');
        $this->abortIf(
            !$this->connection->getDatabasePlatform() instanceof \Doctrine\DBAL\Platforms\MySQL57Platform,
            "Migration can only be executed safely on '\Doctrine\DBAL\Platforms\MySQL57Platform'."
        );

        $this->addSql('DROP TABLE todos');
    }
}
