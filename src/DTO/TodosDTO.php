<?php

namespace App\DTO;

use App\Entity\Author;
use App\Entity\Todos;
use Symfony\Component\Serializer\SerializerInterface;

class TodosDTO
{
    private SerializerInterface $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    public function jsonToTodos(string $data)
    {
        return $this->serializer->deserialize($data, Todos::class, 'json');
    }

    public function getTodosListAsArrayByEntities(array $todos)
    {
        $result = [];
        /** @var Todos $todo */
        foreach ($todos as $todo) {
            $result[] = $this->entityToArray($todo);
        }

        return $result;
    }

    public function entityToArray(Todos $todo)
    {
        return [
            'id' => $todo->getId(),
            'title' => $todo->getTitle(),
            'description' => $todo->getDescription(),
            'status' => $todo->getStatus(),
            'priority' => $todo->getPriority(),
            'parent' => $todo->getParent() instanceof Todos ? $todo->getParent()->getId() : null,
            'author' => $todo->getAuthor() instanceof Author ? $todo->getAuthor()->getId() : null,
        ];
    }
}
