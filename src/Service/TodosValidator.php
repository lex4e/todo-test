<?php

namespace App\Service;

use App\Entity\Todos;
use App\Entity\Status;
use App\Repository\TodosRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class TodosValidator
{
    private $storage;
    private $todosRepository;

    public function __construct(TokenStorageInterface $storage, TodosRepository $todosRepository)
    {
        $this->storage = $storage;
        $this->todosRepository = $todosRepository;
    }

    public function validate(Todos $entity)
    {
        $user = $this->storage->getToken() != null ? $this->storage->getToken()->getUser() : null;

        if (!$user || !$entity->getAuthor() || $user->getId() != $entity->getAuthor()->getId()) {
            throw new \Exception('It is not your task!', Response::HTTP_FORBIDDEN);
        }
    }

    public function validateBeforeUpdate(Todos $entity)
    {
        $this->validate($entity);

        foreach ($this->todosRepository->findByParent($entity->getId()) as $todo) {
            if ($todo->getStatus() !== Status::DONE->value){
                $this->throwBadStatusTodos($entity);
            }
        }
    }

    public function validateBeforeRemove(Todos $entity)
    {
        $this->validate($entity);

        if ($entity->getStatus() === Status::DONE->value) {
            $this->throwBadStatusTodos($entity);
        }

        foreach ($this->todosRepository->findByParent($entity->getId()) as $todo) {
            if ($todo->getStatus() === Status::DONE->value){
                $this->throwBadStatusTodos($entity);
            }
        }
    }

    public function throwBadStatusTodos(Todos $entity)
    {
        throw new \Exception('No permission for this operation! Task #' . $entity->getId()
            . ' have status ' . $entity->getStatus() . '!', Response::HTTP_FORBIDDEN);
    }
}
