<?php

namespace App\Listener;

use App\Repository\TodosRepository;
use App\Service\TodosValidator;
use Doctrine\ORM\Event\LifecycleEventArgs;
use App\Entity\Todos;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class TodosListener
{
    private TokenStorageInterface $storage;
    private TodosRepository $em;
    private TodosValidator $validator;

    public function __construct(TokenStorageInterface $storage, TodosRepository $em, TodosValidator $validator)
    {
        $this->storage = $storage;
        $this->em = $em;
        $this->validator = $validator;
    }
    
    /**
     * @param LifecycleEventArgs $args
     * @throws \Exception
     */
    public function preUpdate(LifecycleEventArgs $args)
    {
        $this->validator->validateBeforeUpdate($args->getEntity());
    }
    
    /**
     * @param LifecycleEventArgs $args
     * @throws \Exception
     */
    public function preRemove(LifecycleEventArgs $args)
    {
        $this->validator->validateBeforeRemove($args->getEntity());
    }
    
    /**
     * @param LifecycleEventArgs $args
     * @return null
     * @throws \Exception
     */
    public function prePersist(LifecycleEventArgs  $args)
    {
        $entity = $args->getEntity();

        if (!$entity instanceof Todos) return null;

        if ($entity->getParent() instanceof Todos) {
            $id = $entity->getParent()->getId();
            $entity->setParent($this->em->find($id));
        }

        $author = $this->storage->getToken()->getUser();
        $entity->setAuthor($author);
        $entity->setCreated(new \DateTime('now'));
    }
}
