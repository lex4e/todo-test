<?php

namespace App\Repository;

use App\Entity\Todos;
use App\Entity\Status;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Todos>
 *
 * @method Todos|null find($id, $lockMode = null, $lockVersion = null)
 * @method Todos|null findOneBy(array $criteria, array $orderBy = null)
 * @method Todos[]    findAll()
 * @method Todos[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TodosRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Todos::class);
    }

    public function add(Todos $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function update(Todos $entity): void
    {
        $this->getEntityManager()->merge($entity);
        $this->getEntityManager()->flush();
    }

    public function save(Todos $entity): void
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
    }

    public function remove(Todos $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
    
    public function delete(int $id): void
    {
        $entity = $this->find($id);
        $this->getEntityManager()->remove($entity);
        $this->getEntityManager()->flush();
    }
    
    
   /**
    * @return Todos[] Returns an array of Todos objects
    */
   public function findByParams(?int $author, ?string $status, ?string $title, ?int $priority, ?string $sortBy): array
   {
        $query = $this->createQueryBuilder('t');

        if ($author) {
            $query = $query->where('t.author = :author')->setParameter('author', $author);
        }

        if ($status && in_array(strtoupper($status), [Status::DONE->value, Status::TODO->value])) {
            $query = $query->andWhere('t.status = :status')->setParameter('status', strtoupper($status));
        }

        if ($title) {
            $query = $query->andWhere('t.title LIKE :title')
                ->setParameter('title', '%' . trim($title) . '%');
        }

        if (isset($priority) && !is_array($priority)) {
             $priorities = explode(',', $priority);
             switch (sizeof($priorities)) {
                 case 2:
                     list($from, $to) = $priorities;
                     $query = $query->andWhere('t.priority >= :from AND t.priority <= :to')
                         ->setParameter('from', intval($from))
                         ->setParameter('to', intval($to));
                     break;
                 case 1:
                     $query = $query->andWhere('t.priority = :priority')
                         ->setParameter('priority', intval($priority));
                     break;
             }
        }

        if (!in_array($sortBy, ['created', 'priority'])) $sortBy = 'created';

        return $query->orderBy('t.' . $sortBy, 'ASC')->getQuery()->getResult();
   }

   public function findByParent(int $parentId)
   {
        return $this->findBy(['parent' => $parentId]);
   }
}
