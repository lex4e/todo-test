<?php

namespace App\Controller;

use App\DTO\TodosDTO;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\TodosRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

#[Route("/api/todos")]
class TodosController extends AbstractController
{
    #[Route('/', name: 'app_todos', methods: ['GET'])]
    public function index(Request $request, TodosRepository $repository, TodosDTO $DTO): JsonResponse
    {
        $data = $request->query->all();

        $todos = $repository->findByParams(
            $data['author'] ?? null,
            $data['status'] ?? null,
            $data['title'] ?? null,
            (int)$data['priority'] ?? null,
            $data['sort_by'] ?? null);

        return $this->json([
            'result' => 'success',
            'data' => $DTO->getTodosListAsArrayByEntities($todos)
        ]);
    }
    
    #[Route('/', name: 'app_todos_create', methods: ['POST'])]
    public function create(Request $request, TodosRepository $repository, TodosDTO $DTO): JsonResponse
    {
        $todo = $DTO->jsonToTodos($request->getContent());
        $repository->save($todo);

        return $this->json([
            'result' => 'success',
            'data' => $DTO->entityToArray($todo)
        ]);
    }
    
    #[Route('/{id}', name: 'app_todos_view', methods: ['GET'])]
    public function view(int $id, TodosRepository $repository, TodosDTO $DTO): JsonResponse
    {
        return $this->json([
            'result' => 'success',
            'data' => $DTO->entityToArray($repository->find($id))
        ]);
    }

    #[Route('/{id}/children', name: 'app_todos_children', methods: ['GET'])]
    public function children(int $id, TodosRepository $repository, TodosDTO $DTO): JsonResponse
    {
        return $this->json([
            'result' => 'success',
            'data' => $DTO->getTodosListAsArrayByEntities($repository->findByParent($id))
        ]);
    }

    #[Route('/', name: 'app_todos_update', methods: ['PUT'])]
    public function update(Request $request, TodosDTO $DTO, TodosRepository $repository)
    {
        $todo = $DTO->jsonToTodos($request->getContent());
        $repository->update($todo);

        return $this->json(['result' => 'success', 'data' => $DTO->entityToArray($todo)]);
    }

    #[Route('/{id}', name: 'app_todos_delete', methods: ['DELETE'])]
    public function delete(int $id, TodosRepository $repository)
    {
        $repository->delete($id);
        
        return $this->json(['result' => 'success']);
    }
}
