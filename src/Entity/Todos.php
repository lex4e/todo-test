<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TodosRepository::class)]
class Todos
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'string', length: 127)]
    private string $title;

    #[ORM\Column(type: 'text')]
    private string $description;

    #[ORM\Column(type: "string", enumType: Status::class)]
    private Status $status;

    #[ORM\Column(type: 'integer', nullable: true)]
    private int $priority;

    #[ORM\ManyToOne(targetEntity: self::class)]
    private Todos $parent;

    #[ORM\OneToMany(mappedBy: 'parent', cascade: ["persist", "remove"], targetEntity: self::class)]
    private Collection $todos;

    #[ORM\ManyToOne(targetEntity: Author::class)]
    private Author $author;
    
    #[ORM\Column(type: 'datetime', nullable: true)]
    private \DateTime $created;

    public function __construct()
    {
        $this->todos = new ArrayCollection();
        $this->status = Status::TODO;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status->name;
    }

    public function setStatus($status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getPriority(): ?int
    {
        return $this->priority;
    }

    public function setPriority(?int $priority): self
    {
        $this->priority = $priority;

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Collection<int, self>
     */
    public function getTodos(): Collection
    {
        return $this->todos;
    }

    public function addTodo(self $todo): self
    {
        if (!$this->todos->contains($todo)) {
            $this->todos[] = $todo;
            $todo->setParent($this);
        }

        return $this;
    }

    public function removeTodo(self $todo): self
    {
        if ($this->todos->removeElement($todo)) {
            // set the owning side to null (unless already changed)
            if ($todo->getParent() === $this) {
                $todo->setParent(null);
            }
        }

        return $this;
    }
    
    public function getAuthor()
    {
        return $this->author;
    }
    
    public function setAuthor(?Author $author): self
    {
        $this->author = $author;
        
        return $this;
    }

    public function getCreated(): ?\DateTime
    {
        return $this->created;
    }
    
    public function setCreated(\DateTime $created): self
    {
        $this->created = $created;
        
        return $this;
    }
}
