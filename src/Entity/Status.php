<?php

namespace App\Entity;

enum Status: string
{
    case TODO = 'TODO';
    case DONE = 'DONE';
}
