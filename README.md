"token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE2NTQ1MjU4OTUsImV4cCI6MTY1NzExNzg5NSwicm9sZXMiOlsiUk9MRV9VU0VSIl0sInVzZXJuYW1lIjoiQXV0aG9yMSJ9.opV6Y2xWmxPK_9Z4hYRc5nqQ5fDSqxW5D1rgVPvXqfuTLaAZdcssz1xLrznTmrUkPDrPIHHEGQMNWCFkVJhY__NZu_7mQOr395leK9-YnzglDATdgYXZJa3BHwT116AOaLW92RFynz9CRiRJTr75FwSVnporJPeShdBHp6ScjmCxBvK42ln32VKljQzOJH3oBd4pFcCbserEKnXF7DpD8MXQuT95jY7zs3n67MrSAseqHRmCxUMeaQ9r2XJnr23AAEdJUcwL_-_1loeYeBO803dqNKwBIAHvfBvpDetboAcKvmNVzNqN3X01TaUgOiCjY-9_zJAu5yD65wsMsjXtjGhee889foj0uhH8mUKk35QiZrIwDkmy59tTnDBJ17sBO5CbOdL1WjxevjqRP2r2NSvIAkjIpldSm5R3epqVsbAukNoyCwFE0n4gl0zo07ZQi1Udti1U_-Mad1a21hBwoUuDT5wa-Qh-L26f2TC17bpA7wJCcrTr0_dPQvrJ2RM1wpAKAtAzt_GE7MkA2fki2Ky9m4AS4kc64_8KYNFkqRWT7ZI_fYtaOm_8mnvq8DSPPPnCH3ZLbPO6uOOpA2oop7UTraBytgXdXq44UJYcsCvEjbLAy6YIi7zHoRfcO8n2U9RmXqy6h5ouUAiR7wxtvVzS514BDmNrqdBCvvRGCr0"


INSTALLATION

1. copy .env.example to .env

2. composer install

3. php bin/console doctrine:migrations:migrate

4. php -S 127.0.0.1:3000


ENDPOINTS EXAMPLES

1. GET http://127.0.0.1:3000/api/todos/?title=T&priority=1,5&status=todos&sort_by=priority

Response: 
{
    "result": "success",
    "data": [
        {
            "id": 9,
            "title": "T9",
            "description": "d5",
            "status": "TODO",
            "priority": 1,
            "parent": 8,
            "author": 1
        },
        {
            "id": 10,
            "title": "T10",
            "description": "d5",
            "status": "TODO",
            "priority": 1,
            "parent": null,
            "author": 1
        }
    ]
}        


2. POST http://127.0.0.1:3002/api/todos/

Body (Request):

{
	"title": "T10",
	"description": "d5",
	"status": "TODOS",
	"priority": 1,
	"parent": {
		"id": 3
	}
}

Response:

{
    "result": "success",
    "data": {
        "id": 37,
        "title": "T8888777__2",
        "description": "d5",
        "status": "DONE",
        "priority": 5,
        "parent": 7,
        "author": 1
    }
}



3. PUT http://127.0.0.1:3002/api/todos

Body (Request):

{
	"id": 9,
	"title": "T10",
	"description": "d5",
	"status": "TODOS",
	"priority": 1,
	"parent": {
		"id": 3
	}
}

Response:

{
    "result": "success",
    "data": {
        "id": 37,
        "title": "T8888777__2",
        "description": "d5",
        "status": "DONE",
        "priority": 5,
        "parent": 7,
        "author": 1
    }
}

4. GET http://127.0.0.1:3002/api/todos/8

Response: 

{
    "result": "success",
    "data": {
        "id": 7,
        "title": "T777",
        "description": "d5",
        "status": "TODO",
        "priority": 3,
        "parent": null,
        "author": 1
    }
}        

5. GET http://127.0.0.1:3002/api/todos/8/children

Response: 

{
    "result": "success",
    "data": [
        {
            "id": 8,
            "title": "T8888",
            "description": "d5",
            "status": "TODO",
            "priority": 5,
            "parent": 7,
            "author": 1
        },
        {
            "id": 13,
            "title": "T8888",
            "description": "d5",
            "status": "TODO",
            "priority": 5,
            "parent": 7,
            "author": 1
        }
    ]
}        

6. DELETE http://127.0.0.1:3002/api/todos/7

Response:

{
    "result": "success"
}
